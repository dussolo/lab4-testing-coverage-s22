package com.hw.db.DAO;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;
import org.junit.jupiter.api.Test;
import org.mockito.AdditionalMatchers;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

class ForumDAOTest {
    @Test
    void testUserListSlugOnly() {
        JdbcTemplate dbWire = Mockito.mock(JdbcTemplate.class);

        new ForumDAO(dbWire);

        ForumDAO.UserList("slug", null, null, null);

        Mockito.verify(dbWire).query(
                Mockito.eq(
                        "SELECT nickname,fullname,email,about FROM forum_users" +
                                " WHERE forum = (?)::citext ORDER BY nickname;"),
                AdditionalMatchers.aryEq(new String[]{"slug"}),
                Mockito.any(UserDAO.UserMapper.class)
        );
    }

    @Test
    void testUserListSlugLimited() {
        JdbcTemplate dbWire = Mockito.mock(JdbcTemplate.class);

        ForumDAO forumDAO = new ForumDAO(dbWire);

        ForumDAO.UserList("slug", 20, null, null);

        Mockito.verify(dbWire).query(
                Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users" +
                        " WHERE forum = (?)::citext ORDER BY nickname LIMIT ?;"),
                AdditionalMatchers.aryEq(new Object[]{"slug", 20}),
                Mockito.any(UserDAO.UserMapper.class)
        );
    }

    @Test
    void testUserListDesc() {
        JdbcTemplate dbWire = Mockito.mock(JdbcTemplate.class);

        ForumDAO forumDAO = new ForumDAO(dbWire);

        ForumDAO.UserList("slug", null, null, true);

        Mockito.verify(dbWire).query(
                Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users" +
                        " WHERE forum = (?)::citext ORDER BY nickname desc;"),
                AdditionalMatchers.aryEq(new String[]{"slug"}),
                Mockito.any(UserDAO.UserMapper.class)
        );
    }

    @Test
    void testUserListSinceAsc() {
        JdbcTemplate dbWire = Mockito.mock(JdbcTemplate.class);

        ForumDAO forumDAO = new ForumDAO(dbWire);

        ForumDAO.UserList("slug", null, "g.freeman", false);

        Mockito.verify(dbWire).query(
                Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users" +
                        " WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"),
                AdditionalMatchers.aryEq(new String[]{"slug", "g.freeman"}),
                Mockito.any(UserDAO.UserMapper.class)
        );
    }

    @Test
    void testUserListSinceDesc() {
        JdbcTemplate dbWire = Mockito.mock(JdbcTemplate.class);

        ForumDAO forumDAO = new ForumDAO(dbWire);

        ForumDAO.UserList("slug", null, "g.freeman", true);

        Mockito.verify(dbWire).query(
                Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users" +
                        " WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;"),
                AdditionalMatchers.aryEq(new String[]{"slug", "g.freeman"}),
                Mockito.any(UserDAO.UserMapper.class)
        );
    }

    @Test
    void testUserListSinceNoOrder() {
        JdbcTemplate dbWire = Mockito.mock(JdbcTemplate.class);

        ForumDAO forumDAO = new ForumDAO(dbWire);

        ForumDAO.UserList("slug", null, "g.freeman", null);

        Mockito.verify(dbWire).query(
                Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users" +
                        " WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"),
                AdditionalMatchers.aryEq(new String[]{"slug", "g.freeman"}),
                Mockito.any(UserDAO.UserMapper.class)
        );
    }
}

