package com.hw.db.DAO;

import com.hw.db.models.User;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

public class UserDAOTest {
    @Test
    void changeNoFields() {
        JdbcTemplate dbWire = Mockito.mock(JdbcTemplate.class);

        new UserDAO(dbWire);

        UserDAO.Change(new User("g.freeman", null, null, null));

        Mockito.verify(dbWire, Mockito.never()).update(Mockito.anyString(), Mockito.any(Object[].class));
    }

    @Test
    void changeAllFields() {
        JdbcTemplate dbWire = Mockito.mock(JdbcTemplate.class);

        new UserDAO(dbWire);

        UserDAO.Change(new User("g.freeman", "g.freeman@blackmesa.com", "Gordon Freeman", "Everybody knows him"));

        Mockito.verify(dbWire).update(
                Mockito.eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq("g.freeman@blackmesa.com"),
                Mockito.eq("Gordon Freeman"),
                Mockito.eq("Everybody knows him"),
                Mockito.eq("g.freeman")
        );
    }

    @Test
    void changeEmail() {
        JdbcTemplate dbWire = Mockito.mock(JdbcTemplate.class);

        new UserDAO(dbWire);

        UserDAO.Change(new User("g.freeman", "g.freeman@blackmesa.com", null, null));

        Mockito.verify(dbWire).update(
                Mockito.eq("UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq("g.freeman@blackmesa.com"),
                Mockito.eq("g.freeman")
        );
    }

    @Test
    void changeFullname() {
        JdbcTemplate dbWire = Mockito.mock(JdbcTemplate.class);

        new UserDAO(dbWire);

        UserDAO.Change(new User("g.freeman", null, "Gordon Freeman", null));

        Mockito.verify(dbWire).update(
                Mockito.eq("UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq("Gordon Freeman"),
                Mockito.eq("g.freeman")
        );
    }

    @Test
    void changeAbout() {
        JdbcTemplate dbWire = Mockito.mock(JdbcTemplate.class);

        new UserDAO(dbWire);

        UserDAO.Change(new User("g.freeman", null, null, "Everybody knows him"));

        Mockito.verify(dbWire).update(
                Mockito.eq("UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq("Everybody knows him"),
                Mockito.eq("g.freeman")
        );
    }
}
