package com.hw.db.DAO;

import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;
import java.time.Instant;

public class PostDAOTest {
    private void mockGetPostWithPost(JdbcTemplate dbWire, Post post) {
        Mockito.when(dbWire.queryForObject(
                Mockito.anyString(),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.anyInt()
        )).thenReturn(post);
    }

    @Test
    void setPostNoChanges() {
        JdbcTemplate dbWire = Mockito.mock(JdbcTemplate.class);

        mockGetPostWithPost(dbWire, new Post());

        new PostDAO(dbWire);

        PostDAO.setPost(0, new Post());

        Mockito.verify(dbWire, Mockito.never()).update(Mockito.anyString(), Mockito.<Object>any());
    }

    @Test
    void setPostChangeAllFields() {
        JdbcTemplate dbWire = Mockito.mock(JdbcTemplate.class);
        Instant ts = Instant.now();

        Post oldPost = new Post();
        oldPost.setAuthor("g.freeman");
        oldPost.setMessage("Don't you worry");
        oldPost.setCreated(Timestamp.from(ts));

        mockGetPostWithPost(dbWire, oldPost);

        new PostDAO(dbWire);

        Post newPost = new Post();
        newPost.setAuthor("e.vance");
        newPost.setMessage("Run everybody!");
        newPost.setCreated(Timestamp.from(ts.plusSeconds(480)));

        PostDAO.setPost(0, newPost);

        Mockito.verify(dbWire).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.eq("e.vance"),
                Mockito.eq("Run everybody!"),
                Mockito.eq(Timestamp.from(ts.plusSeconds(480))),
                Mockito.eq(0)
        );
    }

    @Test
    void setPostChangeMessage() {
        JdbcTemplate dbWire = Mockito.mock(JdbcTemplate.class);
        Instant ts = Instant.now();

        Post oldPost = new Post();
        oldPost.setAuthor("g.freeman");
        oldPost.setMessage("Don't you worry");
        oldPost.setCreated(Timestamp.from(ts));

        mockGetPostWithPost(dbWire, oldPost);

        new PostDAO(dbWire);

        Post newPost = new Post();
        newPost.setMessage("Run everybody!");

        PostDAO.setPost(0, newPost);

        Mockito.verify(dbWire).update(
                Mockito.eq("UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"),
                Mockito.eq("Run everybody!"),
                Mockito.eq(0)
        );
    }

    @Test
    void setPostChangeCreated() {
        JdbcTemplate dbWire = Mockito.mock(JdbcTemplate.class);
        Instant ts = Instant.now();

        Post oldPost = new Post();
        oldPost.setAuthor("g.freeman");
        oldPost.setMessage("Don't you worry");
        oldPost.setCreated(Timestamp.from(ts));

        mockGetPostWithPost(dbWire, oldPost);

        new PostDAO(dbWire);

        Post newPost = new Post();
        newPost.setCreated(Timestamp.from(ts.plusSeconds(480)));

        PostDAO.setPost(0, newPost);

        Mockito.verify(dbWire).update(
                Mockito.eq("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.eq(Timestamp.from(ts.plusSeconds(480))),
                Mockito.eq(0)
        );
    }
}
