package com.hw.db.DAO;

import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

public class ThreadDAOTest {
    @Test
    void treeSortWithLimitSince() {
        JdbcTemplate dbWire = Mockito.mock(JdbcTemplate.class);

        new ThreadDAO(dbWire);

        ThreadDAO.treeSort(0, 10, 0, null);

        Mockito.verify(dbWire).query(
                Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(0),
                Mockito.eq(0),
                Mockito.eq(10)
        );
    }

    @Test
    void treeSortWithLimitSinceAsc() {
        JdbcTemplate dbWire = Mockito.mock(JdbcTemplate.class);

        new ThreadDAO(dbWire);

        ThreadDAO.treeSort(0, 10, 0, false);

        Mockito.verify(dbWire).query(
                Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(0),
                Mockito.eq(0),
                Mockito.eq(10)
        );
    }

    @Test
    void treeSortWithLimitSinceDesc() {
        JdbcTemplate dbWire = Mockito.mock(JdbcTemplate.class);

        new ThreadDAO(dbWire);

        ThreadDAO.treeSort(0, 10, 0, true);

        Mockito.verify(dbWire).query(
                Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(0),
                Mockito.eq(0),
                Mockito.eq(10)
        );
    }

    @Test
    void treeSortWithSinceAsc() {
        JdbcTemplate dbWire = Mockito.mock(JdbcTemplate.class);

        new ThreadDAO(dbWire);

        ThreadDAO.treeSort(0, null, 0, false);

        Mockito.verify(dbWire).query(
                Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(0),
                Mockito.eq(0)
        );
    }

    @Test
    void treeSortWithLimitAsc() {
        JdbcTemplate dbWire = Mockito.mock(JdbcTemplate.class);

        new ThreadDAO(dbWire);

        ThreadDAO.treeSort(0, 10, null, false);

        Mockito.verify(dbWire).query(
                Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  ORDER BY branch LIMIT ? ;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(0),
                Mockito.eq(10)
        );
    }
}
